#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
void tDigest()
{
        unsigned char sm3_value[EVP_MAX_MD_SIZE]; 
        int sm3_len, i;
        EVP_MD_CTX *sm3ctx; 
        sm3ctx = EVP_MD_CTX_new();
        char msg1[10000] = ""; 
        FILE *file;
        char line[32];
        char *ret;
        file = fopen("class.txt", "r");
        if (!file) {
                printf("文件打开失败!\n");
                return 1;
        }
        while (ret = fgets(line, sizeof(line), file)) {
                strcat(msg1,line);
        }        printf("class.txt中的内容为\n%s\n",msg1);
        fclose(file);
        EVP_MD_CTX_init(sm3ctx);
        EVP_DigestInit_ex(sm3ctx, EVP_sm3(), NULL);
        EVP_DigestUpdate(sm3ctx, msg1, strlen(msg1));
        EVP_DigestFinal_ex(sm3ctx, sm3_value, &sm3_len);
        EVP_MD_CTX_reset(sm3ctx);
        printf("class.txt的摘要值为:\n");
        for(i = 0; i < sm3_len; i++)
        {
                printf("0x%02x ", sm3_value[i]);
        }
        printf("\n");
}
int main()
{
        OpenSSL_add_all_algorithms();
        tDigest();
        return 0;
}

